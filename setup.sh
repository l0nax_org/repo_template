#!/bin/bash

# delete Setup Script && 'docs' Directory
rm -rf setup.sh docs/

echo -e "Enter Remote ULR [leave blank for local repo]: \c"
read remoteURL

echo -e "Tag Commit, after finished? [Y|n]: \c"
read tagYN

# delete git Folder
rm -rf .git .gitmodules

## Initiale Git
git init > /dev/null 2>&1

# check if Repo is a Remote Repo
if [ ! -z $remoteURL ]; then
	git remote add origin $remoteURL > /dev/null 2>&1
fi

# create Issue/ Merge Request Templates?
echo -e "[i] Create (gitlab) Issue/ Merge Request Templates? [Y|n]: \c"
read gitlab_Def

if [ ! $gitlab_Def = "Y" ]; then
	rm -rf .gitlab
fi

# replace Date in 'CHANGELOG.md'
date=$(date +%Y-%m-%d)
sed -e "s/{{DATE}}/${date}/g" CHANGELOG.md > .CHANGELOG.md.tmp
rm CHANGELOG.md
mv .CHANGELOG.md.tmp CHANGELOG.md

# get Project Name
echo -e "[i] Project Name: \c"
read PRJ_NAME

## create Scripts settings File
echo "export PRJ_NAME=\"${PRJ_NAME}\"" > .settings

## Changelog
#mkdir -p changelogs/unreleased/

## Documentation
echo -e "Enable Documentation? [y]: \c"
read enableDoc
#
#if [ -z "$enableDoc" ]; then
#	enableDoc="y"
#fi
#
#if [ "$enableDoc" = "y" ]; then
#	./_setup/generateDocs.sh
#
#	# move 'docs' Directory
#	mv _setup/docs/ ./
#fi

mkdir docs

## create README File
echo "# $PRJ_NAME" > README.md
echo "**ToDo/ Coming Soon**" >> README.md

echo ""
echo ""
echo "[i] Doing Git Stuff...."

## remove settings File
rm .settings

## init Submodule
git submodule init

# add 'tools' Submodule
git submodule add https://gitlab.com/l0nax/version_System tools

# update
git submodule update --remote --merge

rm -rf _setup

# Add all Files
git add .
git add .gitlab
git add .gitmodules
git add tools
git commit -m "Initial commit"

if [ "$tagYN" = "Y" ]; then
	git tag v1.0.0
fi

## set Upstream
if [ ! -z $remoteURL ]; then
	git push --set-upstream origin master
fi

## Push Tags
if [ "$tagYN" = "Y" ]; then
	if [ ! -z $remoteURL ]; then
		git push --tags
	fi
fi
