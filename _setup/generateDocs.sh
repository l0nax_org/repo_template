#!/bin/bash
#===============================================================================
#
#          FILE:  generateDocs.sh
# 
#         USAGE:  ./generateDocs.sh <projectName>
# 
#   DESCRIPTION:  Prepare the Docs Directory
# 
#       OPTIONS:  ---
#  REQUIREMENTS:  ---
#          BUGS:  ---
#         NOTES:  ---
#        AUTHOR:  Francesco Emanuel Bennici (), benniciemanuel78@gmail.com
#       COMPANY:  l0nax
#       VERSION:  1.0
#       CREATED:  23.05.2018 00:25:20 CEST
#      REVISION:  ---
#===============================================================================

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

## get Settings
. ${DIR}/../.settings

### === FUNCTIONS ===
function enableMultiLingual() {
	## create LANGS.md File
	local file="${DIR}/docs/LANGS.md"

	echo "# Languages" > $file
	echo "* [English](en/)" >> $file
	echo "* [Deutsch](de/)" >> $file

	## create Directorys
	mkdir ${DIR}/docs/{en,de}
}

function disableAPI() {
	## remove API Directory
	rm -rf ${DIR}/docs/API/
}


### === START ===

echo 
echo "***** Documentation Settings *****"

## get Settings

echo -e "Enable the API Documentation? (Yn):\c"
read enableAPI

echo -e "Enable the External API doc or use the GitBook API Plugin? (E=External; P=Plugin): \c"
read docExPl

if [[ $enableAPI = "N" ]] || [[ $enableAPI = "n" ]]; then
	disableAPI
else
	## add "API" Page to SUMMARY List
	echo "* [API](_api.md)" >> ${DIR}/docs/SUMMARY.md

	## create '_api.md' Page
	
	echo "# API Documentation" >> ${DIR}/docs/_api.md
	if [[ $docExPl = "E" ]] || [[ $docExPl = "e" ]]; then
		echo "[Click here to go to the API Documentation](API/index.html)" >> ${DIR}/docs/_api.md
	fi
fi

echo -e "Enable Multi Lingual? (Yn):\c"
read enableMultiLingual

if [[ $enableMultiLingual = "Y" ]] || [[ $enableMultiLingual = "y" ]]; then
	enableMultiLingual

	echo "    [i] Languages added: 'en': 'English'; 'de': 'Deutsch'"

	## copy 'README.md' File to 'en/' & 'de/'
	cp ${DIR}/docs/README.md ${DIR}/docs/en/
        cp ${DIR}/docs/README.md ${DIR}/docs/de/

	## copy 'SUMMARY.md' File to 'en/' & 'de/'
	cp ${DIR}/docs/SUMMARY.md ${DIR}/docs/en/
        cp ${DIR}/docs/SUMMARY.md ${DIR}/docs/de/

	if [[ $docExPl = "E" ]] || [[ $docExPl = "e" ]]; then
		## Create '_api.md' File for 'en/' & 'de/'
		# english
		echo "# API Documentation" > ${DIR}/docs/en/_api.md
		echo "[Click here to go to the API Documentation](API/index.html)" >> ${DIR}/docs/en/_api.md

		# german
		echo "# API Dokumentation" > ${DIR}/docs/de/_api.md
		echo "[Hier klicken um zu API Dokumentation zu gelangen](API/index.html)" >> ${DIR}/docs/de/_api.md
	fi

	# delete 'README.md', 'SUMMARY.md' & '_api.md' from Docs Root Directory
	rm ${DIR}/docs/README.md
	rm ${DIR}/docs/SUMMARY.md
	rm ${DIR}/docs/_api.md

	######### API #########
	if [[ $enableAPI = "Y" ]] || [[ $enableAPI = "y" ]]; then
		if [[ $docExPl = "E" ]] || [[ $docExPl = "e" ]]; then
			# copy 'API' Folder into Language Folders
			cp -r ${DIR}/docs/API ${DIR}/docs/en/
			cp -r ${DIR}/docs/API ${DIR}/docs/de/
		fi

		## delete API Folder (external)
		rm -rf ${DIR}/docs/API
	fi
fi

## generating 'book.json'
echo -e "{\n\t\"title\":\"${PRJ_NAME}\"," > ${DIR}/docs/book.json
echo -e "\t\"plugins\": [\"theme-api\", \"advanced-emoji\", \"hints\", \"codetabs\"\c" >> ${DIR}/docs/book.json

if [[ $docExPl = "P" ]] || [[ $docExPl = "p" ]]; then
	echo -e ", \"api\"]" >> ${DIR}/docs/book.json
else
	echo "]" >> ${DIR}/docs/book.json
fi

echo "}" >> ${DIR}/docs/book.json


## add Entrys to '.gitignore'
echo "    [i] Creating '.gitignore' for 'docs/' Directory"
echo "_book/*" >> ${DIR}/docs/.gitignore
echo "node_modules/" >> ${DIR}/docs/.gitignore
echo "package-lock.json" >> ${DIR}/docs/.gitignore
echo "package.json" >> ${DIR}/docs/.gitignore

## end
echo "**********************************"
