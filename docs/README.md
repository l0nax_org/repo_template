# Repo Template Documentation

## How to Use
Example Workflow:
```
# Cloning Git Repository
git clone https://gitlab.com/l0nax_org/repo_template.git myNewRepo
cd myNewRepo

# Execute Setup Script
./setup.sh
```
_Example Output from setup.sh:_
```
Enter Remote ULR [leave blank for local repo]: 

...
...

Leeres Git-Repository in /tmp/myNewRepo/.git/ initialisiert
[master (Basis-Commit) 3695e2f] Initial commit
 6 files changed, 218 insertions(+)
 create mode 100644 .gitignore
 create mode 100644 .gitmodules
 create mode 100644 CHANGELOG.md
 create mode 100644 CONTRIBUTING.md
 create mode 100644 README.md
 create mode 100755 setup.sh
...
```

## Used Software
### Documentation
For the normal Documentation we use [gitbook](gitbook.com).<br>
And if you create a API Documentation for you Project use [api blueprint](apiblueprint.org).<br>
