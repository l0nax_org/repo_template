**Language/ Sprache:**<br/>
[English/ Englisch](#English)
[Deutsch/ German](#Deutsch) :warning: **Die deutsche Contributing Guide wird nicht so aktiv geführt wie die Englische!**

# English
<!-- Guide: https://mozillascience.github.io/working-open-workshop/contributing/ -->
## Code Style Guide
In this project the code style of [Google](https://google.github.io/styleguide/cppguide.html) is used.

## Changelog

Please generate the Changlog with the [changelog-Tool](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/bin/changelog) from GitLab.<br>
For this please Read the [developer Documentation](docs/developement/changelog.md)

## Tags/ Version
A label should always be structured according to the following scheme:
**v[VERSION](#Version Number)**
Example:
```
v1.4.2-RC
```

## Version Number
```
v2.3.5-ALPHA
 | | | └────── evtl. Addition
 │ │ └──────── Patch Level
 │ └────────── Minornumber
 └──────────── Majornumber
```

### Major
Indexes extremely significant changes to the program - for example, if the program has been completely rewritten (for example, GIMP 2. x after version 1. x) or if interface compatibility cannot be maintained for libraries.

### Minor
Refers to the functional extension of the program.
e. g. When a new function has been added.

### Patch Level
Contains bug fixes.

### Build
Indicates the progress of the development work in single steps, i. e. starting with 0001, it is incremented by one, starting with every compilation of the code.  Version 5.0.0.0-3242 stands for the 3242th compilation product of a software.  If version control systems are used, instead of the build number, a number is often used which uniquely identifies the sources for compilation within the version control system.  This makes it easier to find the corresponding sources in case of an error.
The build number also shows how many commits have passed since the last day. Set automatically by the build system.

### Revision
The revision number represents the number of all existing commits in the current branch. Set automatically by the build system.

### Addition
Please use the following Additions:<br>
**ALPHA**: during software development, very early stage<br> 
**BETA**:  intended for testing, limited number of users<br>
**RC**:    Publishing candidate, final test version<br>
Please do not use an Addition for (Final) Release or Patch!

## Change important files
If important files (such as [CONTRIBUTING.md][],[README.md][],[LICENSE][],[CHANGELOG.md][], etc.) are changed **the commit must be signed**!

## Make a Release
Please read first about [Version Number](#Version Number).<br/>
The Git Flow model is used.<br/>
Further information: [Git Flow Branching Model][]<br/>

## Workflow Labels

To allow for asynchronous issue handling, we use milestones
and labels. Leads and product managers handle most of the
scheduling into milestones. Labelling is a task for everyone.

Most issues will have labels for at least one of the following:
- Type: ~"feature proposal", ~bug, ~customer, etc.
- Subject: ~wiki, ~"container registry", ~ldap, ~api, ~frontend, etc.
- Team: ~"CI/CD", ~Discussion, ~Quality, ~Platform, etc.
- Milestone: ~Deliverable, ~Stretch, ~"Next Patch Release"
- Priority: ~P1, ~P2, ~P3, ~P4
- Severity: ~S1, ~S2, ~S3, ~S4

### Type labels (~"feature proposal", ~bug, ~customer, etc.)

Type labels are very important. They define what kind of issue this is. Every
issue should have one or more.

Examples of type labels are ~"feature proposal", ~bug, ~customer, ~security,
and ~"direction".

A number of type labels have a priority assigned to them, which automatically
makes them float to the top, depending on their importance.

Type labels are always lowercase, and can have any color, besides blue (which is
already reserved for subject labels).

The descriptions on the labels page explain what falls under each type label.

### Subject labels (~wiki, ~"container registry", ~ldap, ~api, etc.)

Subject labels are labels that define what area or feature of GitLab this issue
hits. They are not always necessary, but very convenient.

If you are an expert in a particular area, it makes it easier to find issues to
work on. You can also subscribe to those labels to receive an email each time an
issue is labeled with a subject label corresponding to your expertise.

Examples of subject labels are ~wiki, ~"container registry", ~ldap, ~api,
~issues, ~"merge requests", ~labels, and ~"container registry".

Subject labels are always all-lowercase.

### Team labels (~"CI/CD", ~Discussion, ~Quality, ~Platform, etc.)

Team labels specify what team is responsible for this issue.
Assigning a team label makes sure issues get the attention of the appropriate
people.

The current team labels are ~Distribution, ~"CI/CD", ~Discussion, ~Documentation, ~Quality,
~Geo, ~Gitaly, ~Monitoring, ~Platform, ~Release, ~"Security Products" and ~"UX".

The descriptions on the labels page explain what falls under the
responsibility of each team.

Within those team labels, we also have the ~backend and ~frontend labels to
indicate if an issue needs backend work, frontend work, or both.

Team labels are always capitalized so that they show up as the first label for
any issue.

### Milestone labels (~Deliverable, ~Stretch, ~"Next Patch Release")

Milestone labels help us clearly communicate expectations of the work for the
release. There are three levels of Milestone labels:

- ~Deliverable: Issues that are expected to be delivered in the current
  milestone.
- ~Stretch: Issues that are a stretch goal for delivering in the current
  milestone. If these issues are not done in the current release, they will
  strongly be considered for the next release.
- ~"Next Patch Release": Issues to put in the next patch release. Work on these
  first, and add the "Pick Into X" label to the merge request, along with the
  appropriate milestone.

Each issue scheduled for the current milestone should be labeled ~Deliverable
or ~"Stretch". Any open issue for a previous milestone should be labeled
~"Next Patch Release", or otherwise rescheduled to a different milestone.

### Bug Priority labels (~P1, ~P2, ~P3, ~P4)

Bug Priority labels help us define the time a ~bug fix should be completed. Priority determines how quickly the defect turnaround time must be.
If there are multiple defects, the priority decides which defect has to be fixed immediately versus later.
This label documents the planned timeline & urgency which is used to measure against our actual SLA on delivering ~bug fixes.

| Label | Meaning         | Estimate time to fix                                             | Guidance |
|-------|-----------------|------------------------------------------------------------------|----------|
| ~P1   | Urgent Priority | The current release + potentially immediate hotfix to GitLab.com |  |
| ~P2   | High Priority   | The next release                                                 |  |
| ~P3   | Medium Priority | Within the next 3 releases (approx one quarter)                  |  |
| ~P4   | Low Priority    | Anything outside the next 3 releases (approx beyond one quarter) | The issue is prominent but does not impact user workflow and a workaround is documented  |

### Bug Severity labels (~S1, ~S2, ~S3, ~S4)

Severity labels help us clearly communicate the impact of a ~bug on users.

| Label | Meaning           | Impact of the defect                                  | Example |
|-------|-------------------|-------------------------------------------------------|---------|
| ~S1   | Blocker           | Outage, broken feature with no workaround             | Unable to create an issue. Data corruption/loss. Security breach. |
| ~S2   | Critical Severity | Broken Feature, workaround too complex & unacceptable | Can push commits, but only via the command line. |
| ~S3   | Major Severity    | Broken Feature, workaround acceptable                 | Can create merge requests only from the Merge Requests page, not through the Issue. |
| ~S4   | Low Severity      | Functionality inconvenience or cosmetic issue         | Label colors are incorrect / not being displayed. |

#### Severity impact guidance

| Label | Security Impact                                                     | Availability / Performance Impact                            |
|-------|---------------------------------------------------------------------|--------------------------------------------------------------|
| ~S1   | >50% users impacted (possible company extinction level event)       |                                                              |
| ~S2   | Many users or multiple paid customers impacted (but not apocalyptic)| The issue is (almost) guaranteed to occur in the near future |
| ~S3   | A few users or a single paid customer impacted                      | The issue is likely to occur in the near future              |
| ~S4   | No paid users/customer impacted, or expected impact within 30 days  | The issue _may_ occur but it's not likely                    |

### Label for community contributors (~"Accepting Merge Requests")

Issues that are beneficial to our users, 'nice to haves', that we currently do
not have the capacity for or want to give the priority to, are labeled as
~"Accepting Merge Requests", so the community can make a contribution.

Community contributors can submit merge requests for any issue they want, but
the ~"Accepting Merge Requests" label has a special meaning. It points to
changes that:

1. We already agreed on,
1. Are well-defined,
1. Are likely to get accepted by a maintainer.

We want to avoid a situation when a contributor picks an
~"Accepting Merge Requests" issue and then their merge request gets closed,
because we realize that it does not fit our vision, or we want to solve it in a
different way.

We add the ~"Accepting Merge Requests" label to:

- Low priority ~bug issues (i.e. we do not add it to the bugs that we want to
solve in the ~"Next Patch Release")
- Small ~"feature proposal"
- Small ~"technical debt" issues

### Issue weight

Issue weight allows us to get an idea of the amount of work required to solve
one or multiple issues. This makes it possible to schedule work more accurately.

You are encouraged to set the weight of any issue. Following the guidelines
below will make it easy to manage this, without unnecessary overhead.

1. Set weight for any issue at the earliest possible convenience
1. If you don't agree with a set weight, discuss with other developers until
consensus is reached about the weight
1. Issue weights are an abstract measurement of complexity of the issue. Do not
relate issue weight directly to time. This is called [anchoring](https://en.wikipedia.org/wiki/Anchoring)
and something you want to avoid.
1. Something that has a weight of 1 (or no weight) is really small and simple.
Something that is 9 is rewriting a large fundamental part of GitLab,
which might lead to many hard problems to solve. Changing some text in GitLab
is probably 1, adding a new Git Hook maybe 4 or 5, big features 7-9.
1. If something is very large, it should probably be split up in multiple
issues or chunks. You can simply not set the weight of a parent issue and set
weights to children issues.

## Pull Request
For a pull request to be accepted, it must be ensured that the processed program code is well documented!<br/>
To improve the readability of the commit, they should be "created" with the following scheme (title):<br/>
A <bold>*</bold> for a change. (Example: ````* Edited README````)_<br/>
A <bold>+</bold> for a new file. (Example: ````+ Added File `'src/health.cpp'````)_<br/>
A <bold>-</bold> for removing a file. (Example: ```- Removed File 'include/health_old.h'````)_<br/>
A <bold>!</bold> for important information. (Example: ```! Fixed Merge Request Error````)_<br/>
<br/>
The "content" of the commit should then be continued with the same scheme, but much more Details.

## Commit Messages

### The seven rules of a great Git commit message
1. Separate subject from body with a blank line
2. Limit the subject line to 50 characters
3. Capitalize the subject line
4. Do not end the subject line with a period
5. Use the imperative mood in the subject line
6. Wrap the body at 72 characters
7. Use the body to explain **what** and **why** vs. **how**

**Example:**
```
Summarize changes in around 50 characters or less

More detailed explanatory text, if necessary. Wrap it to about 72
characters or so. In some contexts, the first line is treated as the
subject of the commit and the rest of the text as the body. The
blank line separating the summary from the body is critical (unless
you omit the body entirely); various tools like `log`, `shortlog`
and `rebase` can get confused if you run the two together.

Explain the problem that this commit is solving. Focus on why you
are making this change as opposed to how (the code explains that).
Are there side effects or other unintuitive consequences of this
change? Here's the place to explain them.

Further paragraphs come after blank lines.

 - Bullet points are okay, too

 - Typically a hyphen or asterisk is used for the bullet, preceded
   by a single space, with blank lines in between, but conventions
   vary here

If you use an issue tracker, put references to them at the bottom,
like this:

Resolves: #123
See also: #456, #789
```

#### 1. Separate subject from body with a blank line
From the `git commit` [manpage](https://mirrors.edge.kernel.org/pub/software/scm/git/docs/git-commit.html#_discussion):
> Though not required, it’s a good idea to begin the commit message with a single short (less than 50 character) line summarizing the change, followed by a blank line and then a more thorough description. The text up to the first blank line in a commit message is treated as the commit title, and that title is used throughout Git. For example, Git-format-patch(1) turns a commit into email, and it uses the title on the Subject line and the rest of the commit in the body.

Not every Commit does requires both a subject and a body.
Sometimes a single line is fine, especially when the change is so simple that no further context is necessary.
For example:
```
Fix typo in introduction to user guide
```

Nothing more need be said;
if the reader wonders what the typo was, she can simply take a look at
the change itself, i.e. use `git show` or `git diff` or `git log -p`.

When a commit merits a bit of explanation and context, you need to write a body.
For example:
```
Derezz the master control program

MCP turned out to be evil and had become intent on world domination.
This commit throws Tron's disc into MCP (causing its deresolution)
and turns it back into a chess game.
```

#### 2. Limit the subject line to 50 characters
Keeping subject lines at this length ensures that they are readable,
and forces the author to think for a moment about the most concise way to explain what’s going on.
> Tip: If you’re having a hard time summarizing, you might be committing too many changes at once.
Strive for [atomic commits](https://www.freshconsulting.com/atomic-commits/) (a topic for a separate post).

#### 3. Capitalize the subject line
Begin all subject lines with a capital letter.<br>
<br>
For example:
* <span style="color:green">Accelerate to 88 miles per hour</span>
<br>
Instead of:
* <span style="color:red">accelerate to 88 miles per hour</span>

#### 4. Do not end the subject line with a period
Trailing punctuation is unnecessary in subject lines.
Besides, space is precious when you’re trying to keep them to [50 chars](#2. Limit the subject line to 50 characters) or less.
<br>
For example:
* <span style="color:green">Open the pod bay doors</span>
<br>
Instead of:
* <span style="color:red">Open the pod bay doors.</span>

#### 5. Use the imperative mood in the subject line
Imperative mood just means "spoken or written as if giving a command or instruction".
A few examples:
- Clean your room
- Close the door
- Take out the trash
<br>
Each of the seven rules you’re reading about right now are written in the imperative ("Wrap the body at 72 characters", etc.).
<br>
The imperative can sound a little rude; that’s why we don’t often use it. But it’s perfect for Git commit subject lines.
One reason for this is that **Git itself uses the imperative whenever it creates a commit on your behalf**.
<br>
For example, the default message created when using `git merge` reads:
```
Merge branch 'myfeature'
```

<br>
And when using `git revert`:
```
Revert "Add the thing with the stuff"

This reverts commit cc87791524aedd593cff5a74532befe7ab69ce9d.
```
<br>

So when you write your commit messages in the imperative, you’re following Git’s own built-in conventions.
For example:
- <span style="color:green">Refactor subsystem X for readability</span>
- <span style="color:green">Update getting started documentation</span>
- <span style="color:green">Remove deprecated methods</span>
- <span style="color:green">Release version 1.0.0</span>
<br>

Writing this way can be a little awkward at first.
We’re more used to speaking in the _indicative mood_, which is all about reporting facts.
That’s why commit messages often end up reading like this:
- <span style="color:red">Fixed bug with Y</span>
- <span style="color:red">Changing behavior of X</span>

And sometimes commit messages get written as a description of their contents:
- <span style="color:red">More fixes for broken stuff</span>
- <span style="color:red">Sweet new API methods</span>
<br>

To remove any confusion, here’s a simple rule to get it right every time.
<br>

**A properly formed Git commit subject line should always be able to complete the following sentence:**
- If applied, this commit will <u>your subject line here</u>

For example:
- If applied, this commit will <span style="color:green">refactor subsystem X for readability</span>
- If applied, this commit will <span style="color:green">update getting started documentation</span>
- If applied, this commit will <span style="color:green">remove deprecated methods</span>
- If applied, this commit will <span style="color:green">release version 1.0.0</span>
- If applied, this commit will <span style="color:green">merge pull request #123 from user/branch</span>
<br>

Notice how this doesn’t work for the other non-imperative forms:
- If applied, this commit will <span style="color:red">fixed bug with Y</span>
- If applied, this commit will <span style="color:red">changing behavior of X</span>
- If applied, this commit will <span style="color:red">more fixes for broken stuff</span>
- If applied, this commit will <span style="color:red">sweet new API methods</span>
<br>

> Remember: Use of the imperative is important only in the subject line. You can relax this restriction when you’re writing the body.

#### 6. Wrap the body at 72 characters
Git never wraps text automatically.
When you write the body of a commit message, you must mind its right margin, and wrap text manually.
<br>
The recommendation is to do this at 72 characters,
so that Git has plenty of room to indent text while still keeping everything under 80 characters overall.
<br>
A good text editor can help here.
It’s easy to configure Vim, for example, to wrap text at 72 characters when you’re writing a Git commit.
Traditionally, however, IDEs have been terrible at providing smart support for text wrapping in commit messages
(although in recent versions, IntelliJ IDEA has `finally` `gotten` `better` about this).

#### 7. Use the body to explain what and why vs. how
This [commit from Bitcoin Core](https://github.com/bitcoin/bitcoin/commit/eb0b56b19017ab5c16c745e6da39c53126924ed6)
is a great example of explaining what changed and why:
```
commit eb0b56b19017ab5c16c745e6da39c53126924ed6
Author: Pieter Wuille <pieter.wuille@gmail.com>
Date:   Fri Aug 1 22:57:55 2014 +0200

   Simplify serialize.h's exception handling

   Remove the 'state' and 'exceptmask' from serialize.h's stream
   implementations, as well as related methods.

   As exceptmask always included 'failbit', and setstate was always
   called with bits = failbit, all it did was immediately raise an
   exception. Get rid of those variables, and replace the setstate
   with direct exception throwing (which also removes some dead
   code).

   As a result, good() is never reached after a failure (there are
   only 2 calls, one of which is in tests), and can just be replaced
   by !eof().

   fail(), clear(n) and exceptions() are just never called. Delete
   them.
```
<br>

In most cases, you can leave out details about how a change has been made.
Code is generally self-explanatory in this regard (and if the code is so complex that it needs to be explained in prose,
that’s what source comments are for). Just focus on making clear the reasons why you made the change in the first place—the way
things worked before the change (and what was wrong with that), the way they work now, and why you decided to solve it the way you did.

[Full Article "How to Write a Git Commit Message"](https://chris.beams.io/posts/git-commit/)

<!-- XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX -->
<!-- XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX -->
<!-- XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX -->

# Deutsch
<!-- Guide: https://mozillascience.github.io/working-open-workshop/contributing/ -->
## Code Style Leitfaden
In diesem Projekt wir der Code-Style von [Google](https://google.github.io/styleguide/cppguide.html) verwendet.

## Tags/ Version
Ein Tag sollte immer so aufgebaut sein:
**v[VERSION](#Versions Nummer)**
Beispiel:
```
v1.4.2-RC
```

## Versions Nummer
Die Versionsnummer ist so aufgebaut:
```
v2.3.5-ALPHA
 | | | └────── evtl. Addition
 │ │ └──────── Patch Level
 │ └────────── Minor Nummer
 └──────────── Major Nummer
```

### Major
Indiziert extrem wichtige Änderungen am Programm - z.B. wenn das Programm komplett neu geschrieben wurde (z.B. GIMP 2. x nach Version 1. x) oder wenn die Schnittstellenkompatibilität für Bibliotheken nicht aufrecht erhalten werden kann.

### Minor
Bezieht sich auf die funktionale Erweiterung des Programms.
z. B. Wenn eine neue Funktion hinnzugefügt wurde.

### Patch Level
Beinhaltet Fehlerbehebungen.

### Build
Gibt den Fortschritt der Entwicklungsarbeit in Einzelschritten an, d. h. beginnend mit 0001 wird er um eins erhöht, beginnend mit jeder Kompilierung des Codes.  Version 5.0.0.0.0-3242 steht für das 3242. Kompilierungsprodukt einer Software.  Wenn Versionskontrollsysteme verwendet werden, wird anstelle der Build-Nummer häufig eine Nummer verwendet, die die Quellen für die Kompilierung innerhalb des Versionskontrollsystems eindeutig identifiziert.  Dies erleichtert das Auffinden der entsprechenden Quellen im Fehlerfall.
Die Buildnummer gibt auch  wieder wie viele Commit vergangen sind seit dem letzten Tag. Wird automatisch von dem Build System gesetzt.

### Revision
Die Revisions Nummer gibt die Zahl aller exesiterenden Commits im aktuellen Branch wieder.  Wird automatisch von dem Build System gesetzt.

### Addition
Bitte verwenden Sie die folgenden Zusätze:
**ALPHA**: während der Softwareentwicklung, sehr frühes Stadium
**BETA**: zum Testen vorgesehen, begrenzte Anzahl von Benutzern
**RC**: Verlagskandidat, endgültige Testversion
Bitte verwenden Sie keinen Zusatz für (Final) Release oder Patch!

## Veränderung wichtiger Dateien
Sollten wichtige Dateien (wie z. B. [CONTRIBUTING.md][], [README.md][], [LICENSE][], [CHANGELOG.md][], etc.) geändert werden, **muss** der Commit signiert werden!

## Neue Version (Release)
Bitte lesen Sie zuerst über [Versionsnummer](#Versions Nummer)<br/>
Es wird mit dem Git Flow Modell gearbeitet.<br/>
Weitere Informationen: [Git Flow Branching Model][]<br/>


<!-- ============ -->
[Git Flow Branching Model]: http://nvie.com/posts/a-successful-git-branching-model/
[CONTRIBUTING.md]: CONTRIBUTING.md
[README.md]: README.md
[LICENSE]: LICENSE
[CHANGELOG.md]: CHANGELOG.md
